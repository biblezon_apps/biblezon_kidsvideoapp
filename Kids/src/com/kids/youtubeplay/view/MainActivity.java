package com.kids.youtubeplay.view;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.kids.youtubeplay.R;
import com.kids.youtubeplay.webservice.autoupdatecontrol.AutoUpdateControl;

public class MainActivity extends FragmentActivity {

    Button btn_hymn, btn_story;
    TextView tv_hymn, tv_story;
    Typeface mfont;
    public static String mType = "";
    private AutoUpdateControl mAutoUpdateControl;
    private Activity mActivity;

    @Override
    protected void onCreate(@Nullable Bundle arg0) {
        // TODO Auto-generated method stub
        super.onCreate(arg0);
        setContentView(R.layout.activity_main);
        initViews();
    }

    /**
     * Initial View of this activity
     */
    private void initViews() {
        mActivity = this;
        btn_hymn = (Button) findViewById(R.id.btn_hymn);
        btn_story = (Button) findViewById(R.id.btn_story);
        tv_hymn = (TextView) findViewById(R.id.tv_hymn);
        tv_story = (TextView) findViewById(R.id.tv_story);

        mfont = Typeface.createFromAsset(this.getAssets(), "fonts/mvboli.ttf");
        tv_hymn.setTypeface(mfont);
        tv_story.setTypeface(mfont);

        /**
         * Autoupdate instance
         */
        mAutoUpdateControl = new AutoUpdateControl(mActivity);

        btn_hymn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListActivity.class);
                mType = "video";
                startActivity(intent);
                finish();
            }
        });

        btn_story.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListActivity.class);
                mType = "story";
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mAutoUpdateControl != null)
            mAutoUpdateControl.initReceiver();

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mAutoUpdateControl != null)
            mAutoUpdateControl.removeReceiver();
    }
}