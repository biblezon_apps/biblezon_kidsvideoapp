package com.kids.youtubeplay.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.kids.youtubeplay.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class VideoAdapter extends BaseAdapter implements Filterable{
	ArrayList<String> title_array;
	ArrayList<String> img_array;
	ArrayList<String> name;
	ArrayList<String> by;

	ArrayList<String> title_array_1;
	
	private Context context;
	private LayoutInflater inflater;
	
	public VideoAdapter(Context context, ArrayList<String> title_array, ArrayList<String> img_array, ArrayList<String> name, ArrayList<String>by) {
		//super(context, R.layout.list_item, title_array, img_array, name, by);
		
		inflater = LayoutInflater.from(context);
		this.context=context;
		this.title_array = title_array;
		title_array_1 = title_array;
		this.img_array = img_array;
		this.name = name;
		this.by = by;
	}

	@Override
	public int getCount() {
		return title_array.size();
	}

	@Override
	public Object getItem(int arg0) {
		return title_array.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		if(arg1 == null)
		{
			Log.e("asdasd", "in gteview");
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			arg1 = inflater.inflate(R.layout.list_item, arg2, false);
		}

		ImageView iv = (ImageView) arg1.findViewById(R.id.thumb_image);
		TextView tv = (TextView) arg1.findViewById(R.id.title);
		Typeface mfont1 = Typeface.createFromAsset(context.getAssets(), "fonts/mvboli.ttf");
		tv.setTypeface(mfont1);
		tv.setText(name.get(arg0));
		TextView tv_1 = (TextView) arg1.findViewById(R.id.by);
		tv_1.setTypeface(mfont1);
		tv_1.setText("by "+by.get(arg0));
		Picasso.with(context).load(img_array.get(arg0)).into(iv);

		return arg1;
	}
	@Override
	public Filter getFilter() {
		return  new VideoFilter();
	}

	private class VideoFilter extends Filter {

		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			constraint = constraint.toString().toLowerCase();
			FilterResults results = new FilterResults();

			if (constraint != null && constraint.length() > 0) {
				String[] tit = null;
				int k = 0;
				//Log.e("filter 1", "" + raw_data);
				for (int i = 0; i < title_array_1.size(); i++) {
					Log.e("filter 2", "" + title_array_1);
					String vieo_name = title_array_1.get(i).toString().toLowerCase();

					if (vieo_name.contains(constraint)) {
						tit[k] = vieo_name;
						k++;
					}
				}
				results.count = tit.length;
				results.values = tit;
			} 
			else {
				results.count = title_array_1.size();
				results.values = title_array_1;
				Log.e("filter 4", "" + results);
				Log.e("filter 44", "" + title_array_1);
			}

			return results;
		}

		@SuppressWarnings("unchecked")
		@Override
		protected void publishResults(CharSequence constraint,
				FilterResults results) {
			if (constraint != null && constraint.length() > 0) {

				title_array = (ArrayList<String>) results.values;
				Log.e("filter 5", "" + title_array);
				notifyDataSetChanged();

			} else {
				title_array = title_array_1;
				Log.e("filter 6", "" + title_array);
				notifyDataSetChanged();
			}
		}
	}
}
