package com.kids.youtubeplay.view;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.biblezone.Rest.RestClient;
import com.kids.youtubeplay.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ListActivity extends FragmentActivity {

    ArrayList<String> url_List = new ArrayList<String>();
    ArrayList<String> title_List = new ArrayList<String>();
    ArrayList<String> by_List = new ArrayList<String>();

    ListView lv;
    VideoAdapter adapter;

    TextView tv_title;
    EditText et_srch;
    ImageView img_srh;
    video_list_task vlt;

    String videoId;
    ImageButton full_view;
    RelativeLayout video_layout;
    RelativeLayout container;
    WebView displayYoutubeVideo;


    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_activity);

        lv = (ListView) findViewById(R.id.listView1);
        tv_title = (TextView) findViewById(R.id.textView1);
        Typeface mfont = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/romy.ttf");
        tv_title.setTypeface(mfont);
        et_srch = (EditText) findViewById(R.id.et_1);
        img_srh = (ImageView) findViewById(R.id.imageView2);

        video_layout = (RelativeLayout) findViewById(R.id.video_layout);
        container = (RelativeLayout) findViewById(R.id.video_view);
        full_view = (ImageButton) findViewById(R.id.full_view);

        findViewById(R.id.dummyView).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


        full_view.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

			/*	Intent lVideoIntent = new Intent(null, Uri.parse("ytv://" + videoId), ListActivity.this,
                        OpenYouTubePlayerActivity.class);
				startActivity(lVideoIntent);*/
                if (displayYoutubeVideo != null)
                    displayYoutubeVideo.destroy();


                Intent lVideoIntent = new Intent(ListActivity.this, Webyoutube.class);
                lVideoIntent.putExtra("youtubeID", videoId);
                startActivity(lVideoIntent);
                finish();
            }
        });


        if (checkNewtworkConnection(ListActivity.this)) {
            vlt = new video_list_task();

            if (Build.VERSION.SDK_INT >= 11) {

                vlt.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "");
            } else {
                vlt.execute("");
            }
        } else {
            Toast.makeText(ListActivity.this, "You are not connected to internet.", Toast.LENGTH_LONG).show();
        }

        lv.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

                videoId = getYoutubeVideoId(url_List.get(arg2)).toString();

                if (videoId == null || videoId.trim().equals("")) {
                    return;
                }

                video_layout.setVisibility(View.VISIBLE);
                OpenVideo(videoId);

/*
                YouTubeFragment fragment = new YouTubeFragment();
				Bundle bundle = new Bundle();
				bundle.putString("video_id", videoId);
				fragment.setArguments(bundle);

				getSupportFragmentManager().beginTransaction().replace(R.id.video_view, fragment).commit();
				Intent lVideoIntent = new Intent(null, Uri.parse("ytv://" + videoId), ListActivity.this,
						OpenYouTubePlayerActivity.class);
				startActivity(lVideoIntent);

				Intent lVideoIntent = new Intent(ListActivity.this, Webyoutube.class);
				lVideoIntent.putExtra("youtubeID", videoId);
				startActivity(lVideoIntent);
				*/

            }
        });

        et_srch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (adapter != null) {
                    adapter.getFilter().filter(s.toString());
                }
                /*
                 * else && s.length()>0 {
				 * pur_Adapter.notifyDataSetInvalidated(); }
				 */
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        img_srh.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (et_srch.getVisibility() == (View.VISIBLE)) {
                    et_srch.setText("");
                    et_srch.setVisibility(View.GONE);
                } else {
                    et_srch.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
        if (displayYoutubeVideo != null)
            displayYoutubeVideo.destroy();
        Intent i = new Intent(ListActivity.this, MainActivity.class);
        startActivity(i);
        finish();
    }

    public void OpenVideo(String youtubeID) {

        String frameVideo = "https://www.youtube.com/embed/" + youtubeID + "?autoplay=1";
        final ProgressDialog pd = ProgressDialog.show(ListActivity.this, "", "Please wait, loading your video.", true);

        displayYoutubeVideo = (WebView) findViewById(R.id.youtubewebview);
        displayYoutubeVideo.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                pd.show();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                pd.dismiss();
            }
        });
        WebSettings webSettings = displayYoutubeVideo.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setUseWideViewPort(true);
        displayYoutubeVideo.loadUrl(frameVideo);
    }

    public static String getYoutubeVideoId(String youtubeUrl) {
        String video_id = "";
        if (youtubeUrl != null && youtubeUrl.trim().length() > 0 && youtubeUrl.startsWith("http")) {

            String expression = "^.*((youtu.be" + "\\/)"
                    + "|(v\\/)|(\\/u\\/w\\/)|(embed\\/)|(watch\\?))\\??v?=?([^#\\&\\?]*).*";
            CharSequence input = youtubeUrl;
            Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(input);
            if (matcher.matches()) {
                String groupIndex1 = matcher.group(7);
                if (groupIndex1 != null && groupIndex1.length() == 11)
                    video_id = groupIndex1;
            }
        }
        return video_id;
    }

    public ArrayList<String> getImgArray(ArrayList<String> url) {
        ArrayList<String> img_arr = new ArrayList<String>();
        for (int i = 0; i < url.size(); i++) {
            img_arr.add("http://img.youtube.com/vi/" + getYoutubeVideoId(url.get(i)) + "/0.jpg");
            Log.e("asdasd", "http://img.youtube.com/vi/" + getYoutubeVideoId(url.get(i)) + "/0.jpg");
        }
        return img_arr;
    }

    private String GetVideoType() {
        if (MainActivity.mType.equalsIgnoreCase("video"))
            return "1";
        else
            return "2";

    }

    class video_list_task extends AsyncTask<String, Integer, Integer> {
        ProgressDialog pd;
        JSONObject json;
        String statusMsg = "";
        String statusCode = "";
        String total_pages = "";
        String total_records = "";

        @Override
        protected Integer doInBackground(String... param) {

            String response = "";
            String url = "http://biblezon.com/appapicms/webservices/videolist/" + GetVideoType();
            Log.d("Final url", url.replaceAll(" ", "%20"));
            RestClient client = new RestClient(url.trim().toString().replaceAll(" ", "%20"));

            try {
                //client.AddParam("video_category_id", id);
                client.Execute(RestClient.RequestMethod.GET);
                response = client.getResponse();
                json = new JSONObject(response);
                Log.d("jObject", "" + json);
                statusCode = json.getString("replyCode");
                statusMsg = json.getString("replyMsg").toString();

                if (statusCode.equalsIgnoreCase("success")) {
                    Log.d("statusCode in back", statusCode);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            pd.dismiss();
            Log.d("statusCode in post", statusCode);

            if (statusCode.equalsIgnoreCase("success")) {
                if (json.has("data")) {

                    try {
                        JSONArray j_array = json.getJSONArray("data");
                        Log.d("j_array", "" + j_array);

                        for (int j = 0; j < j_array.length(); j++) {
                            JSONObject video_object = j_array.getJSONObject(j);
                            Log.e("video_array", "" + video_object);

                            String id = video_object.getString("id");
                            url_List.add(video_object.getString("url").toString());
                            title_List.add(video_object.getString("title").toString());
                            by_List.add(video_object.getString("by").toString());

                            Log.e("id", video_object.getString("id"));
                            Log.e("title", video_object.getString("title"));
                            Log.e("url", video_object.getString("url"));
                            Log.e("by", video_object.getString("by"));
                        }
                        adapter = new VideoAdapter(ListActivity.this, url_List, getImgArray(url_List), title_List,
                                by_List);
                        lv.setAdapter(adapter);
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                } else {
                    Log.d("Json ", "not have user data");
                }
            } else {
                // Toast.makeText(BarList.this, statusMsg,
                // Toast.LENGTH_LONG).show();
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = ProgressDialog.show(ListActivity.this, "", "Loading ...");

        }
    }

    public static boolean checkNewtworkConnection(Context context) {
        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if ((mWifi != null && mWifi.isConnected() && mWifi.isAvailable())
                || (netInfo != null && netInfo.isConnected() && netInfo.isAvailable()))
            return true;
        else
            return false;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("video_id", videoId);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            videoId = savedInstanceState.getString("video_id");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

		/*if(videoId != null){
            video_layout.setVisibility(View.VISIBLE);

			YouTubeFragment fragment = new YouTubeFragment();
			Bundle bundle = new Bundle();
			bundle.putString("video_id", videoId);
			fragment.setArguments(bundle);

			getSupportFragmentManager().beginTransaction().replace(R.id.video_view, fragment).commit();
		}*/
    }
}