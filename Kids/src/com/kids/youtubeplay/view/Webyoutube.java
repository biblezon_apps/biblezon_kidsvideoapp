package com.kids.youtubeplay.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.VideoView;

import com.kids.youtubeplay.R;

/**
 * Created by Diseyi on 6/24/2016.
 */
public class Webyoutube extends Activity {


    // Put in your Video URL here
    private String videoUrl;
    // Declare some variables
    private ProgressDialog pDialog;
    VideoView videoView;
    WebView displayYoutubeVideo;
    ImageButton mCloseButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview_activity);
        findViewById(R.id.dummyViewNew).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        String youtubeID = getIntent().getExtras().get("youtubeID").toString();
        String frameVideo = "https://www.youtube.com/embed/" + youtubeID + "?autoplay=1";
        final ProgressDialog pd = ProgressDialog.show(Webyoutube.this, "", "Please wait, loading your video.", true);

        displayYoutubeVideo = (WebView) findViewById(R.id.webview);
        displayYoutubeVideo.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return false;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                pd.show();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                pd.dismiss();
            }
        });
        WebSettings webSettings = displayYoutubeVideo.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setUseWideViewPort(true);
        displayYoutubeVideo.loadUrl(frameVideo);

        mCloseButton = (ImageButton) findViewById(R.id.mCloseButton);
        mCloseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        displayYoutubeVideo.onPause();
    }

    @Override
    protected void onDestroy() {
        displayYoutubeVideo.destroy();
        displayYoutubeVideo = null;
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (displayYoutubeVideo != null)
            displayYoutubeVideo.destroy();
        startActivity(new Intent(this, ListActivity.class));
    }
}
