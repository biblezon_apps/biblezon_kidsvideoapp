package com.kids.youtubeplay.webservice.control;

import android.app.Activity;

import com.android.volley.VolleyError;
import com.kids.youtubeplay.utils.AndroidAppUtils;

/**
 * Webservice API Error Handler
 *
 * @author Anshuman
 */
public class WebserviceAPIErrorHandler {
    /**
     * Instance of This class
     */
    public static WebserviceAPIErrorHandler mErrorHandler;
    /**
     * Debugging TAG
     */
    private String TAG = WebserviceAPIErrorHandler.class.getSimpleName();

    private WebserviceAPIErrorHandler() {
    }

    /**
     * Get Instance of this class
     *
     * @return
     */
    public static WebserviceAPIErrorHandler getInstance() {
        if (mErrorHandler == null)
            mErrorHandler = new WebserviceAPIErrorHandler();
        return mErrorHandler;

    }

    /**
     * Volley Error Handler for only driver list as on error, user should be
     * navigated back to the map screen
     *
     * @param mError
     */
    public String VolleyErrorHandlerReturningString(VolleyError mError,
                                                    Activity mActivity) {
        String error_message = "Error Occurred";
        AndroidAppUtils.showErrorLog(TAG, "VolleyError :" + mError);
        return error_message;
    }
}
