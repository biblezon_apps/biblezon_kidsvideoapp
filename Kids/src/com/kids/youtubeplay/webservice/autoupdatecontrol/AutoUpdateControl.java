package com.kids.youtubeplay.webservice.autoupdatecontrol;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;

import com.kids.youtubeplay.R;
import com.kids.youtubeplay.utils.AndroidAppUtils;
import com.kids.youtubeplay.webservice.VersionCheckAPIHandler;
import com.kids.youtubeplay.webservice.ihelper.WebAPIResponseListener;

import org.json.JSONObject;

/**
 * Created by Anshuman on 8/17/2016.
 */
public class AutoUpdateControl {
    /**
     * Activity Instance
     */
    private Activity mActivity;
    String LATEST_VERSION = "latestVersion";
    String UPDATED_APP_URL = "appURI";
    private DownloadManager downloadManager;
    private long downloadReference;
    private String TAG = AutoUpdateControl.class.getSimpleName();

    public AutoUpdateControl(Activity mActivity) {
        this.mActivity = mActivity;
        new VersionCheckAPIHandler(mActivity, autoUpdateResponseListener());
    }

    /**
     * Initialized autoupdate broadcast receiver
     */
    public void initReceiver() {
        IntentFilter filter = new IntentFilter(
                DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        mActivity.registerReceiver(downloadReceiver, filter);
    }

    /**
     * Remove autoupdate broadcast receiver
     */
    public void removeReceiver() {
        try {
            if (downloadReceiver != null) {
                mActivity.unregisterReceiver(downloadReceiver);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * On Autoupdate API response listener
     *
     * @return
     */
    private WebAPIResponseListener autoUpdateResponseListener() {
        WebAPIResponseListener mListener = new WebAPIResponseListener() {
            @Override
            public void onSuccessResponse(Object... arguments) {
                try {
                    if (arguments.length > 0)
                        onAutoUpdateAPiResponse((JSONObject) arguments[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailResponse(Object... arguments) {

            }

            @Override
            public void onOfflineResponse(Object... arguments) {

            }
        };
        return mListener;
    }

    /**
     * on AutoUpdate API Response
     *
     * @param response
     */
    private void onAutoUpdateAPiResponse(JSONObject response) {
        if (response.has("success")) {
            /* Success of API Response */
            try {
                float mLatestVersion = 0f, mCurrentVersion = 0f;
                try {
                    mLatestVersion = Float.parseFloat(response.getString(LATEST_VERSION));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                final String appURI = response
                        .getString(UPDATED_APP_URL);
                PackageInfo pInfo = null;
                try {
                    pInfo = mActivity.getPackageManager().getPackageInfo(
                            mActivity.getPackageName(), 0);
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
                try {
                    mCurrentVersion = Float.parseFloat(pInfo.versionName);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                AndroidAppUtils.showLog(TAG, "Latest Version :" + mLatestVersion);
                AndroidAppUtils.showLog(TAG, "Current Version :" + mCurrentVersion);

                if (mLatestVersion > mCurrentVersion) {
                    // oh yeah we do need an upgrade, let the user know send
                    // an alert message
                    AlertDialog.Builder builder = new AlertDialog.Builder(
                            mActivity);
                    builder.setMessage(
                            "There is newer version of this application available, click OK to upgrade now?")
                            .setPositiveButton("OK",
                                    new DialogInterface.OnClickListener() {
                                        // if the user agrees to upgrade
                                        public void onClick(
                                                DialogInterface dialog, int id) {
                                            // start downloading the file
                                            // using the download manager
                                            downloadManager = (DownloadManager) mActivity
                                                    .getSystemService(Context.DOWNLOAD_SERVICE);
                                            AndroidAppUtils.showLog(TAG, "appURI :" + appURI);
                                            Uri Download_Uri = Uri
                                                    .parse(appURI);
                                            DownloadManager.Request request = new DownloadManager.Request(
                                                    Download_Uri);
                                            request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI);
                                            request.setAllowedOverRoaming(false);
                                            request.setTitle(mActivity
                                                    .getResources().getString(
                                                            R.string.app_name));
                                            request.setDestinationInExternalFilesDir(
                                                    mActivity,
                                                    Environment.DIRECTORY_DOWNLOADS,
                                                    mActivity.getResources().getString(R.string.app_name)
                                                            + ".apk");
                                            downloadReference = downloadManager
                                                    .enqueue(request);
                                        }
                                    });
                    // show the alert message
                    builder.create().show();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    // broadcast receiver to get notification about ongoing downloads
    private BroadcastReceiver downloadReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            // check if the broadcast message is for our Enqueued download
            long referenceId = intent.getLongExtra(
                    DownloadManager.EXTRA_DOWNLOAD_ID, -1);
            if (downloadReference == referenceId) {
                // start the installation of the latest version
                Intent installIntent = new Intent(Intent.ACTION_VIEW);
                installIntent.setDataAndType(downloadManager
                                .getUriForDownloadedFile(downloadReference),
                        "application/vnd.android.package-archive");
                installIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mActivity.startActivity(installIntent);

            }
        }
    };

}
