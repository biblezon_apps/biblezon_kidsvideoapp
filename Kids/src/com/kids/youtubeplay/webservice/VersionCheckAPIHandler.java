package com.kids.youtubeplay.webservice;

import android.app.Activity;
import android.content.Context;
import android.provider.Settings;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.kids.youtubeplay.application.ApplicationController;
import com.kids.youtubeplay.utils.AndroidAppUtils;
import com.kids.youtubeplay.webservice.control.WebserviceAPIErrorHandler;
import com.kids.youtubeplay.webservice.ihelper.WebAPIResponseListener;

import org.json.JSONObject;

/**
 * check version of application
 *
 * @author Shruti
 */
public class VersionCheckAPIHandler {
    private Activity mActivity;
    private Context context;
    /**
     * Debug TAG
     */
    private String TAG = VersionCheckAPIHandler.class.getSimpleName();
    /**
     * API Response Listener
     */
    private WebAPIResponseListener mResponseListener;
    /**
     * Autoupdate API URL
     */
    private String AUTO_UPDATE_URL = "http://biblezonadmin.com/biblezon/apk/checkversion.php?app=com.kids.youtubeplay&deviceId=";

    /**
     * @param mActivity
     * @param webAPIResponseListener
     */
    public VersionCheckAPIHandler(Activity mActivity,
                                  WebAPIResponseListener webAPIResponseListener) {
        this.mActivity = mActivity;
        this.context = mActivity;
        this.mResponseListener = webAPIResponseListener;
        postAPICall();

    }

    /**
     * Making json object request
     */
    public void postAPICall() {
        /**
         * JSON Request
         */
        String version_url = (AUTO_UPDATE_URL + Settings.Secure
                .getString(mActivity.getContentResolver(),
                        Settings.Secure.ANDROID_ID)).trim();
        AndroidAppUtils.showLog(TAG, "version_url : " + version_url);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.GET,
                version_url, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                AndroidAppUtils.showInfoLog(TAG, "Response :"
                        + response);
                mResponseListener.onSuccessResponse(response);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                WebserviceAPIErrorHandler.getInstance()
                        .VolleyErrorHandlerReturningString(error, mActivity);
                AndroidAppUtils.hideProgressDialog();
                if (mResponseListener != null)
                    mResponseListener.onFailResponse();
            }
        }) {

        };

        // Adding request to request queue
        ApplicationController.getInstance().addToRequestQueue(
                jsonObjReq, VersionCheckAPIHandler.class.getSimpleName());
        // set request time-out
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(1000 * 20,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Canceling request
        // ApplicationController.getInstance().getRequestQueue()
        // .cancelAll(VersionCheckAPIHandler.class.getSimpleName());
    }
}
