package com.biblezone.Rest;

import java.io.InputStream;
import java.io.OutputStream;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.HttpParams;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.widget.Toast;

public class Utils {
	// http://192.168.0.143/droid/stucheck/commits/master

	public static final String SHARED_PREF_NAME = "Stucheck_App";
	// public static final String BASE_URL =
	// "http://192.168.0.102/stu/users/studentCheckins";

	//public static final String BASE_URL = "http://www.support24hour.com/workplace2/stucheck/users/";
	public static final String BASE_URL = "http://162.242.219.143/stucheck/users/";

	public static HttpClient CLIENT = getThreadSafeClient();

	public static final String CHECKIN_URL = BASE_URL + "checkin";
	public static final String CHECKOUT_URL = BASE_URL + "checkout";
	public static final String Notification_URL = BASE_URL + "checkoutPushNotification";
	public static final String STUDENTCHECKINS_URL = BASE_URL+ "studentCheckins";
	public static final String CHANGE_PASSWORD_URL = BASE_URL+"changePassword";

	public static final String SUCCESS = "SUCCESS";
	public static final String ERROR = "ERROR";

	private static final String DEST_FOLDER = "EVENTFEST";
	private static final String PHOTO_FOLDER = "PHOTO";
	private static final String VIDEO_FOLDER = "VIDEO";
	private static final String ARTIST_FOLDER = "ARTIST";

	public static String DEVICE = "SMALL";
	public static int screen_height = 240;
	public static Long reference = -101l;
	public static final String PHOTO_PATH = Environment
			.getExternalStorageDirectory()
			+ "/"
			+ DEST_FOLDER
			+ "/"
			+ PHOTO_FOLDER;

	public static final String PATH = Environment.getExternalStorageDirectory()
			+ "/" + DEST_FOLDER;
	public static final String VIDEO_PATH = Environment
			.getExternalStorageDirectory()
			+ "/"
			+ DEST_FOLDER
			+ "/"
			+ VIDEO_FOLDER;

	public static final String ARTIST_PATH = Environment
			.getExternalStorageDirectory()
			+ "/"
			+ DEST_FOLDER
			+ "/"
			+ ARTIST_FOLDER;

	public static ProgressDialog mProgressDialog;

	public static Uri GROUP_IMAGE_URI;

	public static String GROUP_IMAGE_PATH;

	/**
	 * Show Progress Dialog
	 * 
	 * @param context
	 *            - Activity context
	 * @param Message
	 *            - loading message
	 */
	public static void showDialog(Context context, String message) {

		if (mProgressDialog == null) {
			mProgressDialog = new ProgressDialog(context);
		}

		if (mProgressDialog != null && !mProgressDialog.isShowing()) {
			mProgressDialog.setMessage(message);
			mProgressDialog.setCancelable(false);
			mProgressDialog.show();
		}
	}

	/**
	 * Hide Progress Dialog
	 * 
	 */
	public static void hideDialog() {
		if (mProgressDialog != null && mProgressDialog.isShowing()) {
			mProgressDialog.dismiss();
			mProgressDialog = null;
		}
	}

	/**
	 * Show a simple AlertDialog
	 * 
	 * @param activity
	 *            - Activity reference
	 * @param title
	 *            - String to show as title
	 * @param msg
	 *            - String to show as message
	 */
	public static void showAlert(Context c, String message, String title) {

		AlertDialog.Builder alert = new AlertDialog.Builder(c);
		alert.setTitle(title);
		alert.setMessage(message);

		alert.setPositiveButton(android.R.string.ok, new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});

		alert.show();

	}

	/**
	 * Show a simple AlertDialog
	 * 
	 * @param activity
	 *            - Activity reference
	 * @param msg
	 *            - String to show as message
	 */
	public static void toast(Context c, String msg) {
		Toast.makeText(c, msg, Toast.LENGTH_SHORT).show();
	}

	/**
	 * Check network connection
	 * 
	 * @param context
	 *            - Activity context
	 */
	public static boolean isOnline(Context ctx) {

		ConnectivityManager cm = (ConnectivityManager) ctx
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo ni = cm.getActiveNetworkInfo();
		boolean result = false;
		if (ni != null) {
			if (ni.getState() == NetworkInfo.State.CONNECTED) {
				result = true;
			}
		}
		return result;
	}

	public static void networkAlert(final Context context) {
		AlertDialog.Builder dialog = new AlertDialog.Builder(context);
		dialog.setTitle("Network Error!");
		dialog.setMessage("No Working Internet Connection");
		dialog.setPositiveButton(android.R.string.ok,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface alert, int arg1) {
						alert.dismiss();
						/*
						 * System.runFinalization(); System.exit(0);
						 */
					}
				});
		dialog.create().show();
	}

	public static DefaultHttpClient getThreadSafeClient() {

		DefaultHttpClient client = new DefaultHttpClient();
		ClientConnectionManager mgr = client.getConnectionManager();
		HttpParams params = client.getParams();

		client = new DefaultHttpClient(new ThreadSafeClientConnManager(params,
				mgr.getSchemeRegistry()), params);

		return client;
	}

	public static void CopyStream(InputStream is, OutputStream os) {
		final int buffer_size = 1024;
		try {
			byte[] bytes = new byte[buffer_size];
			for (;;) {
				int count = is.read(bytes, 0, buffer_size);
				if (count == -1)
					break;
				os.write(bytes, 0, count);
			}
		} catch (Exception ex) {
		}
	}

}